const { spawn } = require("child_process");

const [nodePath, filePath, ...args] = process.argv;

let logs = [];
let timer = null;

function manageLog(log) {
  if (!logs.includes(log)) {
    logs = [...logs, log];
  }
  if (timer) clearTimeout(timer);

  timer = setTimeout(() => {
    console.clear();
    console.log(logs.join(""));
  }, 100);
}

function runCmd(cmd) {
  const yarn = spawn("yarn", [cmd]);
  yarn.stdout.on("data", data => manageLog(`${data}`));
  yarn.stderr.on("data", data => manageLog(`${data}`));
}

args.forEach(runCmd);
