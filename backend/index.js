const db = require("@reinvent/database-client").connect();
// const express = require("express");
const express = require("./src/helpers/express");

const port = 3001;
const app = express();

app.use("/user", async function({ method, params }, res) {
  // const logLabel = `  → Response ${method} ${params[0] || "/"}`;
  // console.time(logLabel);
  try {
    const {
      data: { node: defaultUser }
    } = await db.get("user");

    const {
      data: { node: user }
    } = await db.create("users/:id", defaultUser);

    const { data: node } = await db.get("positions");

    res.json(node);
  } catch (error) {
    res.send("Not found");
  }
  // console.timeEnd(logLabel);
});

// app.listen(port, () => console.log(`📦 Backend: http://localhost:${port}\n`));
app.start(port, () => console.log("🚦 Server start on port: " + port));
