var http = require("http");

function express() {
  console.time("😎  Server start in");
  let router = {};
  let memo = {};

  const json = ({ method, url }, res) => value => {
    const memoKey = `${method}${url}`;
    const str = JSON.stringify(value);
    if (!memo[memoKey]) memo = { ...memo, [memoKey]: str };
    res.end(str);
  };

  const send = ({ method, url }, res) => str => {
    const memoKey = `${method}${url}`;
    if (!memo[memoKey]) memo = { ...memo, [memoKey]: str };
    res.end(str);
  };

  async function middleware(req, res) {
    const logLabel = `  → Response ${req.method} ${req.url}`;
    const memoKey = `${req.method}${req.url}`;
    const action = router[req.url];
    const memoData = memo[memoKey];

    console.time(logLabel);

    res.json = json(req, res);
    res.send = send(req, res);

    if (!action) res.end("Method not allowed");
    else {
      if (memoData) res.end(memo[memoKey]);
      else await action(req, res);
    }

    console.timeEnd(logLabel);
  }

  let app = http.createServer(middleware);

  function use(path, action) {
    router = { ...router, [path]: action };
  }

  function start(port, callback) {
    app.listen(port);
    if (callback) callback(port);
    console.timeEnd("😎  Server start in");
  }

  Object.assign(app, { use, start });

  setInterval(() => {
    memo = {};
  }, 10000);

  return app;
}

module.exports = express;
